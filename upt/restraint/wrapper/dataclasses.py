"""Dataclasses definition for upt procotol."""
from dataclasses import dataclass
import pathlib


@dataclass
class ConsoleTask:
    """Store task information retrieved from the console during a restraint execution."""

    task_id: int
    task_name: str
    result: str
    status: str


@dataclass
class TaskExecutionEnvironment:
    """Store common values related to a task for a restraint execution."""

    is_ewd_hit: bool
    is_multihost: bool
    restraint_output_path: pathlib.Path
