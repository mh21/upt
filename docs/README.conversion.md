# Design decisions

In this project, some design decisions were made. This section of the readme explains some of them.

## restraint influence & UPT legacy workflow

restraint binary itself that is used to run tests has Beaker-compatible format.
It is not very feasible to write a lot of code in C to support another format.
So, instead there's `upt legacy convert` subcommand to convert Beaker file (default: `beaker.xml`)
to a provisioning request in yaml that UPT uses.

The important part is that whenever system gets EWD (External Watchdog Abort), the restraint binary quits.
Therefore, it is necessary to split input and run tests in lowest granularity (per recipeSet).
As a result, each recipeSet in `beaker.xml` is run separately. A separate UPT resource group with an
UPT recipeSet inside it is created internally. For Beaker, a single resource group matches a separate
job. As a result, something that would be submitted in as 1 job is separated and *provisioned* as
multiple Beaker jobs.

Therefore, something like:

```xml
<job>
    <recipeSet><!-- Contents of recipeSet1 ... --></recipeSet>
    <recipeSet><!-- Contents of recipeSet2 ... --></recipeSet>
    <recipeSet><!-- Contents of recipeSet3 ... --></recipeSet>
<job>  
```

is split internally in this way:

```xml
<job>
    <recipeSet><!-- Contents of recipeSet1 ... --></recipeSet>
<job>  
```

```xml
<job>
    <recipeSet><!-- Contents of recipeSet2 ... --></recipeSet>
<job>  
```

```xml
<job>
    <recipeSet><!-- Contents of recipeSet3 ... --></recipeSet>
<job>  
```

and finally to:

```xml
!<ProvisionData>
beaker: !<Beaker>
  rgs:
    - !<ResourceGroup>
      job:
      resource_id: ...
      recipeset: !<RecipeSet>
          restraint_xml:
          <?xml version="1.0" encoding="utf-8"?>
          <job>
           <recipeSet><!-- ONLY TASKS of recipeSet1 ... --></recipeSet>
          </job>
          hosts:
            - !<Host>
              hostname: host_to_run_on
              recipe_id: 123
              duration: 2880
              recipe_fill: |-
                <recipe> ... </recipe>
    - !<ResourceGroup>
      job:
      resource_id: ...
      recipeset: !<RecipeSet>
          restraint_xml:
          <?xml version="1.0" encoding="utf-8"?>
          <job>
           <recipeSet><!-- ONLY TASKS of recipeSet2 ... --></recipeSet>
          </job>
          hosts:
            - !<Host>
              hostname: host_to_run_on
              recipe_id: 123
              duration: 2880
              recipe_fill: |-
                <recipe> ... </recipe>
    - !<ResourceGroup>
      job:
      resource_id: ...
      recipeset: !<RecipeSet>
      restraint_xml:
      <?xml version="1.0" encoding="utf-8"?>
      <job>
       <recipeSet><!-- ONLY TASKS of recipeSet2 ... --></recipeSet>
      </job>
          hosts:
            - !<Host>
              hostname: host_to_run_on
              recipe_id: 123
              duration: 2880
              recipe_fill: |-
                <recipe> ... </recipe>
```
