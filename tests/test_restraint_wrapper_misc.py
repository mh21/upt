"""Test case from upt.restraint.wrapper.misc module."""
import pathlib
import unittest

from upt.plumbing.objects import Host
from upt.restraint.file import RestraintTask
from upt.restraint.wrapper import misc
from upt.restraint.wrapper.dataclasses import ConsoleTask
from upt.restraint.wrapper.task_result import TaskResult

from . import utils


class TestRestraintMisc(unittest.TestCase):
    """Test cases for functions related to TaskResult."""

    def test_get_most_recent_task_result(self):
        """Ensure get_most_recent_result works."""
        host = Host({'hostname': 'hostname1', 'recipe_id': 1234})
        # First task
        restraint_task_1 = RestraintTask.create_from_string(
            '<task id="1" name="t1" result="PASS" status="Completed" />'
        )
        console_task_1 = ConsoleTask(task_id=1, task_name='t1',
                                     result='PASS', status='Completed')
        # Second task (running)
        restraint_task_2_0 = RestraintTask.create_from_string(
            '<task id="2" name="t2" result="" status="Running" />'
        )
        console_task_2_0 = ConsoleTask(task_id=2, task_name='t2',
                                       result='', status='Running')
        # Second task (completed)
        restraint_task_2_1 = RestraintTask.create_from_string(
            '<task id="2" name="t2" result="FAIL" status="Completed" />'
        )
        console_task_2_1 = ConsoleTask(task_id=2, task_name='t2',
                                       result='FAIL', status='Completed')
        # Third task
        restraint_task_3 = RestraintTask.create_from_string(
            '<task id="3" name="t3" result="FAIL" status="Completed" />'
        )
        console_task_3 = ConsoleTask(task_id=3, task_name='t3',
                                     result='FAIL', status='Completed')

        task_results = [
            TaskResult(host, restraint_task_1, console_task_1),
            TaskResult(host, restraint_task_2_0, console_task_2_0),
            TaskResult(host, restraint_task_2_1, console_task_2_1),
            TaskResult(host, restraint_task_3, console_task_3),
        ]

        result = misc.get_most_recent_task_result(host.recipe_id, 2, task_results)
        self.assertEqual(task_results[2], result)

        result = misc.get_most_recent_task_result(host.recipe_id, 333, task_results)
        self.assertIsNone(result)

    def test_clean_filename_encoded_as_html(self):
        """Test fix_file_name_encoded_as_html."""
        cases = (
            ('File name without HTML characters', 'El niño.txt', 'El niño.txt'),
            ('File name with HTML characters', 'El%20ni%C3%B1o.txt', 'El niño.txt')
        )
        for description, original_file, expected_file in cases:
            with self.subTest(description), utils.create_temporary_files([original_file]):
                final_file = misc.fix_file_name_encoded_as_html(pathlib.Path(original_file))
                self.assertEqual(final_file.name, expected_file)
